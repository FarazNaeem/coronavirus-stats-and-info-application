package com.automata.statsinfo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final String TAG = MainActivity.class.getSimpleName();
    private InterstitialAd interstitialAd;
    TextView coronaCases, deaths, recovered, mildcases, criticalcases, s1, s2, s3, s4;
    String CoronaCases = "", Deaths = "", Recovered = "", mild = "", critical = "";
    String strong1, strong2, strong3, strong4;
    String urlworldo = "https://www.worldometers.info/coronavirus/";
    String urlusa = "https://www.nytimes.com/interactive/2020/us/coronavirus-us-cases.html";
    private List<Model> countries;
    private List<UsaModel> states;
    private List<StatesModel> CountryStates;
    RecyclerView recyclerView, statesRecycler;
    ProgressBar mprogressBar, mCountryprogressbar, mStatesprogressbar;
    LinearLayout stateslayout, paklayout, usalayout, indialayout, italylayout, spainlayout;
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newdesigin);
        __init__();
        runOnUiThread(new Runnable() {
            public void run() {
                updateStats();
            }
        });
        new doinbackgroud().execute();
        usalayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usalayout.setVisibility(View.GONE);
                new getusadata().execute();
            }
        });
        paklayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new getPakstates().execute();
            }
        });
        indialayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new getIndiastates().execute();
            }
        });
        italylayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new getItalystates().execute();
            }
        });
        spainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new getSpainstates().execute();
            }
        });

        AudienceNetworkAds.initialize(this);
        interstialAds();
    }

    public class getSpainstates extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            stateslayout.setVisibility(View.VISIBLE);
            stateslayout.requestFocus(View.FOCUS_DOWN);
            mStatesprogressbar.setVisibility(View.VISIBLE);
            CountryStates.clear();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            statesRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            statesRecycler.setAdapter(new StatesAdaptor(getApplicationContext(), CountryStates));
            mStatesprogressbar.setVisibility(View.GONE);
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Document document = Jsoup.connect("https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_Spain")
                        .timeout(30 * 1000)
                        .get();
                Element table = document.getElementsByClass("wikitable sortable").first();
                int i = 0;
                int j = 0;
                for (Element tr : table.select("tr")) {
                    if (i > 0) {


                        StatesModel model = new StatesModel();
                        for (Element td : tr.getElementsByTag("td")) {
                            if (j == 0) {
                                model.setStatname(td.text().trim());
                            } else if (j == 1) {
                                model.setTotalcases(td.text().trim());
                            } else if (j == 2) {
                                model.setTotalactivecase(td.text().trim());
                                Log.d("tttt", "doInBackground: " + td.text());
                            } else if (j == 4) {
                                model.setTotaldeaths(td.text().trim());
                                Log.d("tttt", "doInBackground: " + td.text());
                            } else if (j == 5) {
                                model.setTotalrecover(td.text().trim());
                                Log.d("tttt", "doInBackground: " + td.text());
                                break;
                            }
                            j += 1;
                        }
                        j = 0;
                        CountryStates.add(model);
                    }
                    i += 1;
                }


            } catch (UnknownHostException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "no internet", Toast.LENGTH_LONG).show();
                    }
                });

                e.printStackTrace();
            } catch (SocketTimeoutException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "check your network connection", Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            } catch (NullPointerException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "something Went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class getItalystates extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            stateslayout.setVisibility(View.VISIBLE);
            stateslayout.requestFocus(View.FOCUS_DOWN);
            mStatesprogressbar.setVisibility(View.VISIBLE);
            CountryStates.clear();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            statesRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            statesRecycler.setAdapter(new StatesAdaptor(getApplicationContext(), CountryStates));
            mStatesprogressbar.setVisibility(View.GONE);
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Document document = Jsoup.connect("https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_Italy")
                        .timeout(30 * 1000)
                        .get();
                Elements table = document.getElementsByClass("wikitable sortable");
                int i = 0;
                int j = 0;
                for (Element tr : table.select("tr")) {
                    if (i > 1) {
                        StatesModel model = new StatesModel();
                        model.setStatname(tr.getElementsByTag("th").text().trim());
                        for (Element td : tr.getElementsByTag("td")) {
                            if (j == 0) {
                                model.setTotalcases(td.text().trim());
                            } else if (j == 1) {
                                model.setTotaldeaths(td.text().trim());
                                Log.d("tttt", "doInBackground: " + td.text());
                            } else if (j == 3) {
                                model.setTotalactivecase(td.text().trim());
                                Log.d("tttt", "doInBackground: " + td.text());
                            } else if (j == 5) {
                                model.setTotalrecover(td.text().trim());
                                Log.d("tttt", "doInBackground: " + td.text());
                                break;
                            }
                            j += 1;
                        }
                        j = 0;
                        CountryStates.add(model);
                    }
                    i += 1;
                }

            } catch (UnknownHostException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "no internet", Toast.LENGTH_LONG).show();
                    }
                });

                e.printStackTrace();
            } catch (SocketTimeoutException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "check your network connection", Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            } catch (NullPointerException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "something Went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public  class getPakstates extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            stateslayout.setVisibility(View.VISIBLE);
            stateslayout.requestFocus(View.FOCUS_DOWN);
            mStatesprogressbar.setVisibility(View.VISIBLE);
            CountryStates.clear();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            statesRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            statesRecycler.setAdapter(new StatesAdaptor(getApplicationContext(), CountryStates));
            mStatesprogressbar.setVisibility(View.GONE);
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Document document = Jsoup.connect("https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_Pakistan")
                        .timeout(30 * 1000)
                        .get();
                Elements table = document.getElementsByClass("wikitable sortable");
                int i = 0;
                int j = 0;
                for (Element tr : table.select("tr")) {
                    if (i > 1) {
                        StatesModel model = new StatesModel();
                        System.out.println(tr.getElementsByTag("th").text());
                        model.setStatname(tr.getElementsByTag("th").text().trim());
                        for (Element td : tr.getElementsByTag("td")) {
                            if (j == 0) {
                                model.setTotalcases(td.text().trim());
                            } else if (j == 1) {
                                model.setTotaldeaths(td.text().trim());
                                Log.d("tttt", "doInBackground: " + td.text());
                            } else if (j == 2) {
                                model.setTotalrecover(td.text().trim());
                                Log.d("tttt", "doInBackground: " + td.text());
                            } else if (j == 3) {
                                model.setTotalactivecase(td.text().trim());
                                Log.d("tttt", "doInBackground: " + td.text());
                                break;
                            }
                            j += 1;
                        }
                        j = 0;
                        CountryStates.add(model);
                    }
                    i += 1;
                }

            } catch (UnknownHostException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "no internet", Toast.LENGTH_LONG).show();
                    }
                });

                e.printStackTrace();
            } catch (SocketTimeoutException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "check your network connection", Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            } catch (NullPointerException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "something Went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class getIndiastates extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            stateslayout.setVisibility(View.VISIBLE);
            stateslayout.requestFocus(View.FOCUS_DOWN);
            mStatesprogressbar.setVisibility(View.VISIBLE);
            CountryStates.clear();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            statesRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            statesRecycler.setAdapter(new StatesAdaptor(getApplicationContext(), CountryStates));
            mStatesprogressbar.setVisibility(View.GONE);
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Document document = Jsoup.connect("https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_India")
                        .timeout(30 * 1000)
                        .get();
                Elements table = document.getElementsByClass("wikitable plainrowheaders sortable mw-collapsible");
                int i = 0;
                int j = 0;
                for (Element tr : table.select("tr")) {
                    if (i > 1) {
                        StatesModel model = new StatesModel();
                        for (Element elements : tr.getElementsByTag("th")) {
                            if (j > 0) {
                                model.setStatname(elements.text().trim());
                            }
                            j += 1;
                        }
                        j = 0;
                        for (Element td : tr.getElementsByTag("td")) {
                            if (j == 0) {
                                model.setTotalactivecase(td.text().trim());
                            } else if (j == 1) {
                                model.setTotaldeaths(td.text().trim());
                                Log.d("tttt", "doInBackground: " + td.text());
                            } else if (j == 2) {
                                model.setTotalrecover(td.text().trim());
                                Log.d("tttt", "doInBackground: " + td.text());
                            } else if (j == 3) {
                                model.setTotalcases(td.text().trim());
                                Log.d("tttt", "doInBackground: " + td.text());
                                break;
                            }
                            j += 1;
                        }
                        j = 0;
                        CountryStates.add(model);
                    }
                    i += 1;
                }

            } catch (UnknownHostException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "no internet", Toast.LENGTH_LONG).show();
                    }
                });

                e.printStackTrace();
            } catch (SocketTimeoutException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "check your network connection", Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            } catch (NullPointerException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "something Went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class getusadata extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            stateslayout.setVisibility(View.VISIBLE);
            mStatesprogressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            statesRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            statesRecycler.setAdapter(new usaAdaptor(getApplicationContext(), states));
            mStatesprogressbar.setVisibility(View.GONE);
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Document document = Jsoup.connect(urlusa).timeout(30 * 1000)
                        .userAgent("Mozilla")
                        .get();
                Elements element = document.select("tbody");
                int i = 0;
                for (Element tr : element.select("tr")) {
                    UsaModel model = new UsaModel();
                    for (Element td : tr.select("td")) {
                        if (i == 0) {
                            Log.d("usa", "doInBackground: " + td.text());
                            model.setStateName(td.text());
                        } else if (i == 1) {
                            Log.d("usa", "doInBackground: " + td.text());
                            model.setStateCases(td.text());
                        } else if (i == 2) {
                            Log.d("usa", "doInBackground: " + td.text());
                            model.setStateDeaths(td.text());
                            break;
                        }
                        i += 1;
                    }
                    i = 0;
                    states.add(model);
                }


            } catch (UnknownHostException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "no internet", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            } catch (SocketTimeoutException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "check your network connection", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class doinbackgroud extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mprogressBar.setVisibility(View.VISIBLE);
            mCountryprogressbar.setVisibility(View.VISIBLE);

            usalayout.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mprogressBar.setVisibility(View.GONE);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            final myadaptor adaptor = new myadaptor(getApplicationContext(), countries);
            recyclerView.setAdapter(adaptor);
            mCountryprogressbar.setVisibility(View.GONE);
            //linearLayout.setVisibility(View.VISIBLE);
            searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    adaptor.getFilter().filter(s);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    adaptor.getFilter().filter(s);
                    return false;
                }
            });

        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Document document = Jsoup.connect(urlworldo)
                        .timeout(30 * 1000)
                        .get();

                Elements el;
                el = document.select("div.maincounter-number");
                int i = 0;
                for (Element e : el) {
                    if (i == 0) {
                        CoronaCases = e.text();
                    } else if (i == 1) {
                        Deaths = e.text();
                    } else if (i == 2) {
                        Recovered = e.text();
                    }
                    i += 1;
                }
                Elements e;
                e = document.select("span.number-table");
                i = 0;
                for (Element ell : e) {
                    if (i == 0) {
                        mild = ell.text();
                    } else if (i == 1) {
                        critical = ell.text();
                    }
                    i += 1;
                }


                i = 0;
                Elements element = document.select("strong");
                for (Element strong : element) {
                    if (i == 0) {
                        strong1 = strong.text();
                    } else if (i == 1) {
                        strong2 = strong.text();
                    } else if (i == 2) {
                        strong3 = strong.text();
                    } else if (i == 3) {
                        strong4 = strong.text();
                        break;
                    }
                    i += 1;
                }
                runOnUiThread(new Runnable() {
                    public void run() {
                        updateStats();
                    }
                });
                i = 0;
                Element table = document.select("tbody").first();
                for (Element rows : table.select("tr")) {
                    Model m = new Model();
                    for (Element data : rows.select("td")) {
                        if (i == 0) {
                            m.setCountry(data.text());
                        } else if (i == 1) {
                            m.setTotalcases(data.text());
                        } else if (i == 2) {
                            m.setNewcases(data.text());
                        } else if (i == 3) {
                            m.setTotaldeaths(data.text());
                        } else if (i == 4) {
                            m.setNewdeaths(data.text());
                            break;
                        }
                        i += 1;
                    }
                    countries.add(m);
                    i = 0;
                }
            } catch (UnknownHostException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "no internet", Toast.LENGTH_SHORT).show();
                    }
                });

                e.printStackTrace();
            } catch (SocketTimeoutException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "check your network connection", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void updateStats() {
        coronaCases.setText(CoronaCases);
        deaths.setText(Deaths);
        recovered.setText(Recovered);
        mildcases.setText(mild);
        criticalcases.setText(critical);
        s1.setText(strong4 + "%");
        s2.setText(strong1 + "%");
        s3.setText(strong3 + "%");
        s4.setText(strong2 + "%");

    }

    private void interstialAds() {
        interstitialAd = new InterstitialAd(this, "2290443491258087_2290560974579672");
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Interstitial ad is loaded and ready to be displayed
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                // Show the ad
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        });

        // For auto play video ads, it's recommended to load the ad
        // at least 30 seconds before it is shown
        interstitialAd.loadAd();

    }

    public void __init__() {

        countries = new ArrayList<>();
        states = new ArrayList<>();
        CountryStates = new ArrayList<>();

        searchView = findViewById(R.id.searchview);
        searchView.clearFocus();

        usalayout = findViewById(R.id.clickhereUsa);
        paklayout = findViewById(R.id.clickherePak);
        indialayout = findViewById(R.id.clickhereIndia);
        italylayout = findViewById(R.id.clickhereItaly);
        spainlayout = findViewById(R.id.clickhereSpain);

        stateslayout = findViewById(R.id.stateslayout);
        stateslayout.setVisibility(View.GONE);
        mprogressBar = findViewById(R.id.main_progressbar);
        mCountryprogressbar = findViewById(R.id.country_progressbar);
        mStatesprogressbar = findViewById(R.id.states_progressbar);
        mprogressBar.setVisibility(View.GONE);
        mCountryprogressbar.setVisibility(View.GONE);
        mStatesprogressbar.setVisibility(View.GONE);

        recyclerView = findViewById(R.id.countrieslist);
        statesRecycler = findViewById(R.id.stateslist);

        coronaCases = findViewById(R.id.world_cases);
        deaths = findViewById(R.id.world_deaths);
        recovered = findViewById(R.id.world_recover);
        mildcases = findViewById(R.id.world_mild);
        criticalcases = findViewById(R.id.world_critical);

        s1 = findViewById(R.id.strong1);
        s2 = findViewById(R.id.strong2);
        s3 = findViewById(R.id.strong3);
        s4 = findViewById(R.id.strong4);
    }

    public void hideCountrylayouts() {
        paklayout.setVisibility(View.GONE);
        indialayout.setVisibility(View.GONE);
        italylayout.setVisibility(View.GONE);
        usalayout.setVisibility(View.GONE);

    }

    public void showCountrylayouts() {
        paklayout.setVisibility(View.VISIBLE);
        indialayout.setVisibility(View.VISIBLE);
        italylayout.setVisibility(View.VISIBLE);
        usalayout.setVisibility(View.VISIBLE);
    }

}
