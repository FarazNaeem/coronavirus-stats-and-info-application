package com.automata.statsinfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class myadaptor extends RecyclerView.Adapter<myadaptor.myViewHolder> implements Filterable {

    private Context context;
    private List<Model> countries;
    private List<Model> copyCountries;
    public myadaptor(Context context, List<Model> countries){
        this.context=context;
        this.countries=countries;
        this.copyCountries=new ArrayList<>(this.countries);
    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View view=inflater.inflate(R.layout.country_report_new_design,parent,false);
        return new myViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final myViewHolder holder, final int position) {
        holder.countryname.setText(countries.get(position).getCountry());
        holder.countrycases.setText(countries.get(position).getTotalcases()+" Cases");
        holder.countrynewcases.setText(countries.get(position).getNewcases());
        holder.countrydeaths.setText(countries.get(position).getTotaldeaths()+" Deaths");
        holder.countrynewdeaths.setText(countries.get(position).getNewdeaths());
        holder.countrylayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(countries.get(position).getCountry().equals("Pakistan")){

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }
    private Filter filter= new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<Model> filteredList= new ArrayList<>();
            if(charSequence.equals("") || charSequence.length() ==0){
                filteredList.addAll(copyCountries);
            }else{

                String filterpattern=charSequence.toString().toLowerCase().trim();
                for(Model country : copyCountries){
                    if(country.getCountry().toLowerCase().contains(filterpattern)){
                        filteredList.add(country);
                    }
                }
            }
            FilterResults results=new FilterResults();
            results.values=filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            countries.clear();
            countries.addAll((List) filterResults.values);
            notifyDataSetChanged();
        }
    };


    public class myViewHolder  extends  RecyclerView.ViewHolder{
        private TextView countryname,countrycases,countrydeaths,countrynewcases,countrynewdeaths;
        RelativeLayout countrylayout;
        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            countryname=itemView.findViewById(R.id.countryname);
            countrycases=itemView.findViewById(R.id.countrycases);
            countrydeaths=itemView.findViewById(R.id.countrydeaths);
            countrynewcases=itemView.findViewById(R.id.country_new_cases);
            countrynewdeaths=itemView.findViewById(R.id.country_new_deaths);
            countrylayout=itemView.findViewById(R.id.countriesLayout);

        }

    }
}
