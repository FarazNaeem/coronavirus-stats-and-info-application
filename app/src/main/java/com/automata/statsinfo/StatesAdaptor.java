package com.automata.statsinfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class StatesAdaptor extends RecyclerView.Adapter<StatesAdaptor.myViewHolder> {

    Context context;
    private List<StatesModel> countryStates;
    public StatesAdaptor(Context context,List<StatesModel> countryStates){
        this.context=context;
        this.countryStates=countryStates;
    }
    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View view=inflater.inflate(R.layout.states_report,parent,false);
        return  new myViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myViewHolder holder, int position) {
        holder.statename.setText(countryStates.get(position).getStatname());
        holder.statecases.setText(countryStates.get(position).getTotalcases()+" Cases");
        holder.statedeaths.setText(countryStates.get(position).getTotaldeaths()+" Deaths");
        holder.activecases.setText("Active "+countryStates.get(position).getTotalactivecase());
        holder.totalrecover.setText("Recover "+countryStates.get(position).getTotalrecover());
    }

    @Override
    public int getItemCount() {
        return countryStates.size();
    }

    public class myViewHolder  extends  RecyclerView.ViewHolder{
        private TextView statename,statecases,statedeaths,activecases,totalrecover;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            statename=itemView.findViewById(R.id.statesname);
            statecases=itemView.findViewById(R.id.statescases);
            statedeaths=itemView.findViewById(R.id.statesdeaths);
            activecases=itemView.findViewById(R.id.statesnewcases);
            totalrecover=itemView.findViewById(R.id.statesnewdeaths);
        }
    }
}
