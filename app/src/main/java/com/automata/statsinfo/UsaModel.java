package com.automata.statsinfo;

public class UsaModel {
    String stateName,stateCases,stateDeaths;

    public UsaModel(){}
    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateCases() {
        return stateCases;
    }

    public void setStateCases(String stateCases) {
        this.stateCases = stateCases;
    }

    public String getStateDeaths() {
        return stateDeaths;
    }

    public void setStateDeaths(String stateDeaths) {
        this.stateDeaths = stateDeaths;
    }
}
