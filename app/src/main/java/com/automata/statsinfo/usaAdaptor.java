package com.automata.statsinfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class usaAdaptor extends RecyclerView.Adapter<usaAdaptor.myViewHolder>{

    private Context context;
    private List<UsaModel> states;
    public usaAdaptor(Context context, List<UsaModel> states){
        this.context=context;
        this.states=states;
    }
    @NonNull
    @Override
    public usaAdaptor.myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View view=inflater.inflate(R.layout.usa_states,parent,false);
        return  new myViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myViewHolder holder, int position) {
        holder.statename.setText(states.get(position).getStateName());
        holder.statecases.setText(states.get(position).getStateCases()+" Cases");
        holder.statedeaths.setText(states.get(position).getStateDeaths()+" Deaths");
    }


    @Override
    public int getItemCount() {
        return states.size();
    }

    public class myViewHolder  extends  RecyclerView.ViewHolder{
        private TextView statename,statecases,statedeaths;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            statename=itemView.findViewById(R.id.statename);
            statecases=itemView.findViewById(R.id.statecases);
            statedeaths=itemView.findViewById(R.id.statedeaths);



        }

    }
}
