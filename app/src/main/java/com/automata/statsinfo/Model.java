package com.automata.statsinfo;

import android.content.Context;

public class Model  {
    String country,totalcases,newcases,totaldeaths,newdeaths,totalrecovered,activecases,seriouscases;

    public Model() {

    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTotalcases() {
        return totalcases;
    }

    public void setTotalcases(String totalcases) {
        this.totalcases = totalcases;
    }

    public String getNewcases() {
        return newcases;
    }

    public void setNewcases(String newcases) {
        this.newcases = newcases;
    }

    public String getTotaldeaths() {
        return totaldeaths;
    }

    public void setTotaldeaths(String totaldeaths) {
        this.totaldeaths = totaldeaths;
    }

    public String getNewdeaths() {
        return newdeaths;
    }

    public void setNewdeaths(String newdeaths) {
        this.newdeaths = newdeaths;
    }

    public String getTotalrecovered() {
        return totalrecovered;
    }

    public void setTotalrecovered(String totalrecovered) {
        this.totalrecovered = totalrecovered;
    }

    public String getActivecases() {
        return activecases;
    }

    public void setActivecases(String activecases) {
        this.activecases = activecases;
    }

    public String getSeriouscases() {
        return seriouscases;
    }

    public void setSeriouscases(String seriouscases) {
        this.seriouscases = seriouscases;
    }
}
