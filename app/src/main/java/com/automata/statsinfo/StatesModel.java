package com.automata.statsinfo;

public class StatesModel {
String statname,totalcases,totaldeaths,totalrecover,totalactivecase;
public StatesModel(){}

    public String getStatname() {
        return statname;
    }

    public void setStatname(String statname) {
        this.statname = statname;
    }

    public String getTotalcases() {
        return totalcases;
    }

    public void setTotalcases(String totalcases) {
        this.totalcases = totalcases;
    }

    public String getTotaldeaths() {
        return totaldeaths;
    }

    public void setTotaldeaths(String totaldeaths) {
        this.totaldeaths = totaldeaths;
    }

    public String getTotalrecover() {
        return totalrecover;
    }

    public void setTotalrecover(String totalrecover) {
        this.totalrecover = totalrecover;
    }

    public String getTotalactivecase() {
        return totalactivecase;
    }

    public void setTotalactivecase(String totalactivecase) {
        this.totalactivecase = totalactivecase;
    }
}
